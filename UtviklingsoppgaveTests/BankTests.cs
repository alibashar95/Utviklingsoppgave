﻿using Utviklingsoppgave;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UtviklingsoppgaveTests {
    [TestClass]
    public class BankTests {

        private Person ola;
        private Bank bank;

        [TestInitialize]
        public void TestInitialize() {
            ola = new Person("Ola Nordmann", new Money(200));
            bank = new Bank();
        }

        [TestMethod]
        public void CreateAccount_CheckName_DifferentOwners() {
            Person brad = new Person("Brad Pitt", new Money(50000000));
            Account account = bank.CreateAccount(ola, new Money(0));
            Account secondAccount = bank.CreateAccount(brad, new Money(0));

            Assert.AreEqual("olanordmann1", account.AccountName);
            Assert.AreEqual("bradpitt1", secondAccount.AccountName);
        }

        [TestMethod]
        public void CreateAccount_CheckName_TenAccounts() {
            
            Account[] accounts = new Account[10];

            for (int i = 0; i < accounts.Length; i++){
                accounts[i] = bank.CreateAccount(ola, new Money(0));
            }

            for (int i = 0; i < 10; i++) {
                Assert.AreEqual(accounts[i].AccountName, "olanordmann" + (i + 1));
            }
            
        }

        [TestMethod]
        public void CreateAccount_InsufficientFunds_ReturnsNull() {
            Account actual = bank.CreateAccount(ola, new Money(300));
            Assert.AreEqual(null, actual);
        }

        [TestMethod]
        public void GetAccountForCustomer_NoAccounts_ReturnsZero() {
            Person person = new Person("Ola Nordmann", new Money(0));
            Account[] accounts = bank.GetAccountForCustomer(person);
            Assert.AreEqual(0, accounts.Length);
        }

        [TestMethod]
        public void GetAccountForCustomer_TenAccounts() {
            Account[] expectedAccounts = new Account[10];
            for (int i = 0; i < expectedAccounts.Length; i++) {
                expectedAccounts[i] = bank.CreateAccount(ola, new Money(0));
            }

            Account[] actualAccounts = bank.GetAccountForCustomer(ola);

            for (int i = 0; i < 10; i++) {
                Assert.AreEqual(expectedAccounts[i], actualAccounts[i]);
            }
        }

        [TestMethod]
        public void Deposit_Fifty_CheckAccountAndOwner() {
            Account account = bank.CreateAccount(ola, new Money(100));

            bank.Deposit(account, new Money(50));

            Assert.AreEqual(150, account.Balance.Amount);
            Assert.AreEqual(50, ola.Money.Amount);
        }

        [TestMethod]
        public void Deposit_InsufficientFunds_ReturnsOneHundred() {
            Account account = bank.CreateAccount(ola, new Money(100));

            bank.Deposit(account, new Money(150));

            Assert.AreEqual(100, account.Balance.Amount);
        }

        [TestMethod]
        public void Withdraw_Fifty_CheckAccountAndOwner() {
            Account account = bank.CreateAccount(ola, new Money(100));

            bank.Withdraw(account, new Money(50));

            Assert.AreEqual(50, account.Balance.Amount);
            Assert.AreEqual(150, ola.Money.Amount);
        }

        [TestMethod]
        public void Withdraw_InsufficientFunds_ReturnsOneHundred() {
            Account account = bank.CreateAccount(ola, new Money(100));

            bank.Withdraw(account, new Money(200));

            Assert.AreEqual(100, account.Balance.Amount);
        }

        [TestMethod]
        public void Transfer_InsufficientFunds() {
            Account account = bank.CreateAccount(ola, new Money(100));
            Account secondAccount = bank.CreateAccount(ola, new Money(50));

            bank.Transfer(secondAccount, account, new Money(100));

            Assert.AreEqual(100, account.Balance.Amount);
            Assert.AreEqual(50, secondAccount.Balance.Amount);
        }

        [TestMethod]
        public void Transfer_Success() {
            Account account = bank.CreateAccount(ola, new Money(100));
            Account secondAccount = bank.CreateAccount(ola, new Money(50));

            bank.Transfer(account, secondAccount, new Money(100));

            Assert.AreEqual(0, account.Balance.Amount);
            Assert.AreEqual(150, secondAccount.Balance.Amount);
        }

    }
}
