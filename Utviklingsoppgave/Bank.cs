﻿using System;
using System.Collections.Generic;
namespace Utviklingsoppgave {

    public class Bank {

        private List<Account> allAccounts;

        public Bank() {
            allAccounts = new List<Account>();
        }

        public Account CreateAccount(Person customer, Money initialDeposit) {

            if (CheckForEnoughMoney(customer.Money, initialDeposit)) {
                Account newAccount = new Account(customer, initialDeposit);
                customer.Money.Subtract(initialDeposit.Amount);
                allAccounts.Add(newAccount);
                return newAccount;
            }
            
            ShowMessage("Insufficient funds");
            return null;
        }

        public Account[] GetAccountForCustomer(Person customer) {
            return customer.OwnedAccounts.ToArray();
        }

        public void Deposit(Account to, Money amount) {
            if (CheckForEnoughMoney(to.Owner.Money, amount)) {
                to.IncreaseBalance(amount);
                to.Owner.Money.Subtract(amount.Amount);
            } else
                ShowMessage("Insufficient funds");
        }

        public void Withdraw(Account from, Money amount) {
            if (CheckForEnoughMoney(from.Balance, amount)) {
                from.DecreaseBalance(amount);
                from.Owner.Money.Add(amount.Amount);
            } else
                ShowMessage("Insufficient funds");
        }

        public void Transfer(Account from, Account to, Money amount) {
            if (CheckForEnoughMoney(from.Balance, amount)) {
                from.DecreaseBalance(amount);
                to.IncreaseBalance(amount);
            } else
                ShowMessage("Insufficient funds");
        }

        private bool CheckForEnoughMoney(Money from, Money to) {
            if (from.Amount >= to.Amount)
                return true;

            return false;
        }

        private static void ShowMessage(string message) {
            Console.Out.WriteLine(message);
        }

    }
}
