﻿namespace Utviklingsoppgave {
    public class Money {

        private int amount;

        public Money(int amount) {
            this.amount = amount;
        }

        public void Add(int amount) {
            this.amount += amount;
        }

        public void Subtract(int amount) {
            this.amount -= amount;
        }

        // Properties
        public int Amount {
            get { return amount; }
            set { amount = value; }
        }
    }
}