﻿using System.Collections.Generic;

namespace Utviklingsoppgave {
    public class Person {

        private string name;
        private Money money;
        private List<Account> ownedAccounts;

        public Person(string name, Money money) {
            this.name = name;
            this.money = money;
            ownedAccounts = new List<Account>();
        }

        public int GetNumberOfOwnedAccounts() {
            return ownedAccounts.Count;
        }

        public void AddAccount(Account account) {
            ownedAccounts.Add(account);
        }


        // Properties
        public string Name {
            get { return name; }
        }

        public Money Money {
            get { return money; }
            set { money = value; }
        }

        public List<Account> OwnedAccounts {
            get { return ownedAccounts; }
        }

    }
}