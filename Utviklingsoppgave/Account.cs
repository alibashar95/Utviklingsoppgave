﻿using System;

namespace Utviklingsoppgave {
    public class Account {

        private Person owner;
        private string accountName;
        private Money balance;
        private static int numberOfAccountsCreated = 0;

        internal Account(Person owner, Money initialDeposit) {
            this.owner = owner;
            balance = initialDeposit;
            accountName = owner.Name.Replace(" ", String.Empty).ToLower() + (owner.GetNumberOfOwnedAccounts() + 1);
            numberOfAccountsCreated++;
            owner.AddAccount(this);
        }

        public void IncreaseBalance(Money amount) {
            balance.Add(amount.Amount);     
        }

        public void DecreaseBalance(Money amount) {
            balance.Subtract(amount.Amount);
        }

        // Properties
        public Person Owner {
            get { return owner; }
        }

        public string AccountName {
            get { return accountName; }
        }

        public Money Balance {
            get { return balance; }
            set { balance = value; }
        }

        public int NumberOfAccountsCreated {
            get { return numberOfAccountsCreated; }
        }

    }
}